<?php
require 'index.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INICIO</title>

    <style>
		body{background-color: #while; padding:2px; font-family: Arial; }
        #descripcion h4{
            text-align: center;

        }
		
	</style>
</head>
<body id="descripcion">
    <h4 >Mapeo del Impacto</h4>
    <p>                        
        
Entregar una solución para el manejo de datos de una ONG en la que se transforme de un
archivo de texto en formato .xlsx a una base de datos relacional, para mejorar en un 25% de
entrega de la información o certificados.
Actualmente se está registrando información en una Hojas de calculo de Google, sin
embargo, ahora que tienen mas de 5000 registros, la hoja de calculo dificulta el ingreso de
la información, a demás para poder generar los certificados utilizan la herramienta de
Combinación de correspondencia de Microsoft para por correo enviar les sus diplomas. </p>

<h4 >IMPACTO</h4>
<P>
Usuarios de telecapacitación: Mejora en un 25% del tiempo al acceso a su información.
Personal administrativo: Reducir 25% el trabajo repetitivo en la elaboración y envío de
información.
Personal de tecnología: Reducir 20% el trabajo repetitivo y dejar de enfocarse en lo
urgente y diseñar soluciones de valor.
Estudiantes del curso de Aseguramiento de la calidad de software: Crear un proyecto
real a partir de buenas practicas que aseguren la calidad del software, Promocionar
servicios o productos de los estudiantes.
UMG Sistemas Cobán: Presentar la capacidad de ingeniería para solucionar problemas de
la sociedad Alta Verapacense. </p>
</body>
</html>